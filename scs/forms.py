# -*- coding: utf-8 -*-
from models import StorageFile
from django.forms import ModelForm


class StorageFileForm(ModelForm):
    class Meta:
        model = StorageFile
        fields = ['remote_file']
