# -*- coding: utf-8 -*-
from django.contrib import admin
from scs.models import StorageFile


class StorageFileAdmin(admin.ModelAdmin):
    list_display = ('remote_file', 'container_name', 'download_url')


admin.site.register(StorageFile, StorageFileAdmin)
