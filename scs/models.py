from django.db import models
from django.db.models.query import QuerySet
from django.conf import settings
from scs import SCStorage

storage = SCStorage()

SCS_CONTAINER_NAME = getattr(settings, 'SCS_CONTAINER_NAME', None)


class StorageFileManager(models.Manager):
    def get_query_set(self):
        return self.model.QuerySet(self.model)

    def __getattr__(self, attr, *args):
        return getattr(self.get_query_set(), attr, *args)


class StorageFile(models.Model):

    # Default upload folder is the root directory of the container
    remote_file = models.FileField(upload_to='/', storage=storage, db_index=True)

    container_name = models.CharField(
        max_length=255, default=SCS_CONTAINER_NAME, editable=True
    )

    objects = StorageFileManager()

    def __unicode__(self):
        return self.remote_file.name

    def get_files_list(self):
        return self.remote_file.storage.listdir()[1]

    def delete(self, *args, **kwargs):
        # override for remove files from remote storage
        storage, file_name = self.remote_file.storage, self.remote_file.name
        super(StorageFile, self).delete(*args, **kwargs)
        if storage.exists(file_name):
            storage.delete(file_name)

    def download_url(self):
        return self.remote_file.storage.url(self.remote_file)

    class QuerySet(QuerySet):
        # override for properly work of
        # admin delete objects action
        def delete(self):
            for obj in self.all():
                obj.delete()
