# -*- coding: utf-8 -*-
# The middleware used for connect to the remote storage
# and for sync list of files between client and storage
from django.http import HttpResponse
from models import SCStorage, StorageFile


class StorageConnectMiddleware():
    def process_request(self, request):
        try:
            s = SCStorage()
            # syncronise remotes and local file listing
            for next_file in s.listdir()[1]:
                sf_obj, created = StorageFile.objects.get_or_create(
                    remote_file=next_file
                )
            return None
        except Exception as e:
            msg = "An error has occured during connecting to \
                the remote storage. %s." % str(e)
            return HttpResponse(msg)
