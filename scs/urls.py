from django.conf.urls import patterns, url
from scs.views import UploadFileView, IndexView, DeleteFileView


urlpatterns = patterns(
    '',
    url(r'^$', IndexView.as_view(), name='index_view'),
    url(r'^upload/$', UploadFileView.as_view(), name='upload_view'),
    url(r'^delete/(?P<id>[0-9]+)/$', DeleteFileView.as_view(), name='delete_view'),
)
