#-*- coding: utf-8 -*-
""" Simple Backend for Selectel Cloud Storage
https://support.selectel.ru/storage/api_info/
"""
__version__ = "0.1.0"


from django.conf import settings
from django.core.files.base import ContentFile
from django.core.files.storage import Storage
from django.utils.encoding import smart_unicode
from django.core.exceptions import ImproperlyConfigured
import swiftclient.client as client


SCS_AUTH_URL = getattr(settings, 'SCS_AUTH_URL', None)
SCS_AUTH_USER = getattr(settings, 'SCS_AUTH_USER', None)
SCS_AUTH_KEY = getattr(settings, 'SCS_AUTH_KEY', None)
SCS_CONTAINER_NAME = getattr(settings, 'SCS_CONTAINER_NAME', None)
SCS_LINK_DOMAIN = getattr(settings, 'SCS_LINK_DOMAIN', None)


# The case when the container is public are not supported now!
if any([SCS_AUTH_URL, SCS_AUTH_USER, SCS_AUTH_KEY, SCS_CONTAINER_NAME]) is None:
    raise ImproperlyConfigured("Some of required settings are not specified!")


class SCStorage(Storage):

    def __init__(self, auth_url=SCS_AUTH_URL, user=SCS_AUTH_USER,
                 key=SCS_AUTH_KEY, container=SCS_CONTAINER_NAME,
                 domain=SCS_LINK_DOMAIN):
        self.auth_url = auth_url
        self.user = user
        self.key = key
        self.container = container
        self.domain = domain
        # trying to auth
        try:
            self.storage_url, self.token = client.get_auth(
                self.auth_url, self.user, self.key
            )
        except client.ClientException as e:
            raise e
        # check for the container is exists
        try:
            client.get_container(self.storage_url, self.token, self.container)
        except client.ClientException as e:
            raise e

    def _open(self, name, mode='rb'):
        return SCFile(name=name, scs_instance=self)

    def _save(self, name, content):
        f = SCFile(name=name, scs_instance=self)
        f.write(content)
        f.close()
        return f.name

    def delete(self, name):
        try:
            client.delete_object(
                self.storage_url, self.token, self.container, name
            )
        except client.ClientException as e:
            raise e

    def exists(self, name):
        try:
            client.head_object(
                self.storage_url, self.token, self.container, name
            )
            return True
        except client.ClientException:
            return False

    def listdir(self, path=None):
        """
        Lists the contents of the specified path, returning a 2-tuple
        of lists; the first item being directories, the second item
        being files.
        All items are in unicode and starts with "/" !
        """
        dirs = []
        files = []
        # normalize path
        if path and not path.endswith("/"):
            path = "{0}/".format(path)
        if path and path.startswith("/"):
            path = path[1:]
        try:
            container = client.get_container(
                self.storage_url, self.token, self.container
            )
        except client.ClientException as e:
            raise e
        for item in container[1]:
            # not include results from other paths
            if path and not item['name'].startswith(path):
                continue
            if 'application/directory' in item['content_type']:
                dirs.append(smart_unicode(u"/{0}".format(item['name'])))
            else:
                files.append(smart_unicode(u"/{0}".format(item['name'])))
        return (dirs, files)

    def size(self, name):
        """
        Returns the size of the object in bytes.
        """
        try:
            return int(client.head_object(
                self.storage_url, self.token, self.container, name
            )['content-length'])
        except client.ClientException as e:
            raise e

    def url(self, name):
        """
        Returns absolute url to file or raise ImproperlyConfigured
        """
        if self.domain is None:
            raise ImproperlyConfigured(
                "Please set SCS_LINK_DOMAIN for use this feature!"
            )
        return "http://{0}{1}".format(self.domain, name)


try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO


class SCFile(ContentFile):

    def __init__(self, name, scs_instance):
        self.scs = scs_instance
        self.name = name
        self.pos = 0
        self.file = StringIO()
        self.mode = 'rw'

    @property
    def size(self):
        return self.scs.size(self.name)

    def open(self, mode=None):
        if mode is not None:
            self.mode = mode
        return self.file

    def read(self, num_bytes=None):
        if num_bytes is None:
            try:
                self.file = StringIO(
                    client.get_object(
                        self.scs.storage_url,
                        self.scs.token,
                        self.scs.container,
                        self.name
                    )[1]
                )
                return self.file.getvalue()
            except client.ClientException as e:
                raise e
            else:
                raise NotImplementedError("The num of bytes to read")

    def write(self, data):
        if not 'w' in self.mode:
            raise NotImplementedError("ReadOnly mode")
        self.file = StringIO(data.read())
        try:
            client.put_object(
                self.scs.storage_url,
                self.scs.token,
                self.scs.container,
                self.name,
                self.file.getvalue()
            )
        except client.ClientException as e:
            raise e

    def close(self):
        self.pos = 0
        self.file.close()

    def multiple_chunks(self, chunk_size=None):
        return False

    def chunks(chunk_size=None):
        raise NotImplementedError("Chunks")
