# -*- coding: utf-8 -*-
from django.views import generic
from django.shortcuts import render, redirect
from models import StorageFile
from forms import StorageFileForm


class IndexView(generic.ListView):
    context_object_name = 'storage_files'

    def get_queryset(self):
        return StorageFile.objects.all()


class UploadFileView(generic.base.View):
    form_class = StorageFileForm
    template_name = 'scs/upload.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            sf = StorageFile(remote_file=request.FILES['remote_file'])
            sf.save()
            return redirect('index_view')

        return render(request, self.template_name, {'form': form})


class DeleteFileView(generic.base.View):
    def get(self, request, *args, **kwargs):
        file_id = self.kwargs['id']
        StorageFile.objects.filter(id=file_id).delete()
        return redirect('index_view')
